# Kelompok-1_Tugas Besar Sister_FTP (RPC)

#### Nama Anggota:

- Muhammad Fikri Sabriansyah (1301164298)
- Simiao Salvador Da Gama (1301163617)
- Tia Ariska Putri (1301164223)
- Monica Putri (1301164115)

#### Kelas IF 40-12

## Requirement
- Python > 3.7


==============================================================================================

==============================================================================================
### Testing Program (Client Upload, Client Download) & Hasil Upload dan Download Client  : Oleh Tia Ariska
### Testing Program (Server Upload, Server Download) & Hasil Upload dan Download Server : Oleh Monica Putri
==============================================================================================


#### FTP (File Transfer Protocol)


adalah suatu protokol yang berfungsi 
untuk tukar–menukar file dalam suatu 
network yang menggunakan TCP koneksi. 


Dua hal yang penting dalam FTP adalah 
FTP Server dan FTP Client.


#### RPC (Remote Procedure Call)

adalah sebuah metode yang memungkinkan kita untuk mengakses 
sebuah prosedur yang berada di komputer lain. 

Pendekatan yang dilakuan adalah sebuah server membuka socket, 
lalu menunggu client yang meminta prosedur yang disediakan oleh 
server. 

Bila client tidak tahu harus menghubungi port yang mana, client bisa me-request kepada sebuah matchmaker pada sebuah RPC port 
yang tetap. 


### Testing Program


- Server Upload


![](./images_ss/server_upload.JPG)


- Client Upload


![](./images_ss/client_upload.JPG)


- Server Download


![](./images_ss/server_download.JPG)


- Client Download


![](./images_ss/client_download.JPG)


### Hasil Upload dan Download


- Hasil Upload dan Download Server


![](./images_ss/hasil_server_upload.JPG)


- Hasil Upload dan Download Client


![](./images_ss/hasil_upload_download_client.JPG)


==============================================================================================

==============================================================================================

## Update v1.0 by Muhammad Fikri Sabriansyah

### FTP Upload


- Server Upload

Server memilih directory untuk menyimpan file yang akan diupload oleh client

![](./images_ss/server_3.png)


- Client Upload

Client memilih file yang akan diupload ke server

![](./images_ss/client_4.png)


- Hasil Upload Server-Client


![](./images_ss/server_4.png)


### FTP Download

- Server Download

Server memilih file yang akan didownload oleh client


![](./images_ss/server_1.png)


- Client Download

Kondisi awal isi dari folder Downloads

![](./images_ss/client_1.png)


Client memilih directory untuk penyimpan file yang akan didownload dari server

![](./images_ss/client_2.png)


- Hasil Download Server-Client


![](./images_ss/client_3.png)










====================================================================================================================

====================================================================================================================
## Tambahkan FTP untuk Remote Server dari Client : Oleh Simiao Salvador da Gama 

### FTP Remote dari Server
- Start server ftp

![](./images_ss/5s.png)

 Server menunggu sampai diremote oleh Client

- Server berhasil diremote 

![](./images_ss/4s.png)

Server akan selalu memantau kapan dan directory mana yang telah atau sedang  dikunjungi oleh client

- File di server berhasil didownload

![](./images_ss/2s.png)

Terdapat alert ketika client telah mendownload file dari server (finalreport.txt)

### FTP Remote Client
- Start ftp Client

![](./images_ss/1c.png)

- Masuk ke Gui, dan masukan ip server yang akan diremote

![](./images_ss/6c.png)

- Client berhasil remote Server

![](./images_ss/9c.png)

Pada bagian kiri menunjukan semua directory dari server dan pada bagian kanan menunjukan semua directory dari client.

- Download file dari Server

![](./images_ss/5c.png)

Mendownload file finalreport.txt

- File berhasil didownload dan disimpan ke directory yang telah ditentukan

![](./images_ss/8c.png)