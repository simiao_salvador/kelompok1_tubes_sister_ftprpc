# import SimpleXMLRPCServer bagian server
from xmlrpc.server import SimpleXMLRPCServer
import tkinter as tk
from tkinter import filedialog

tes = tk.Tk()
tes.withdraw()
# menentukan directory penyimpanan
loc = filedialog.askdirectory()
loc_final = loc + "/"

# buat fungsi bernama file_upload()
def file_upload(filedata):
    
    # buka file dengan format apa aja
    with open(loc_final+"hasil_upload.*",'wb') as handle:
        #convert from byte to binary IMPORTANT!
        data1=filedata.data
        
        # tulis file tersebut
        handle.write(data1)
        return True  #IMPORTANT
        
# must have return value
# else error messsage: "cannot marshal None unless allow_none is enabled"

# buat server

server = SimpleXMLRPCServer(("127.0.0.1",9999))
# tulis pesan server telah berjalan
print ("Listening on port 9999")

# register fungsi 
server.register_function(file_upload,"file_upload")

# jalankan server
server.serve_forever()