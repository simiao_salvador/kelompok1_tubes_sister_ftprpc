# import library SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer
# import xmlrpc bagian client
import xmlrpc.client
# import selecting file
import tkinter as tk
from tkinter import filedialog
import os

tes = tk.Tk()
tes.withdraw()

# pilih file yang akan dikirim
file = filedialog.askopenfilename(filetypes=(('All files', '.*'),))
loc = os.path.split(file)[0]
nama_file = os.path.split(file)[1]
loc_final = loc + "/"

# buatlah fungsi bernama download()
def file_download():

    # buka file yang akan dikirim
    with open(loc_final+nama_file,'rb') as handle:
        # kirimkan file tersebut dalam bentuk xml dengan cara memanggil xmlrpc.client.Binary()
        return xmlrpc.client.Binary(handle.read())
        

# buat server pada IP dan port yang telah ditentukan
server = SimpleXMLRPCServer(("127.0.01", 8001))

# print bahwa "server mendengarkan pada port xxx"
print ("Listening on port 8001")

# register fungsi download pada server
server.register_function(file_download, 'file_download')

# jalankan server
server.serve_forever()
