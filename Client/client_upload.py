# import xmlrpc bagian client
import xmlrpc.client
# import selecting file
import tkinter as tk
from tkinter import filedialog
import os

# buat stub proxy client
proxy = xmlrpc.client.ServerProxy("http://127.0.0.1:9999/")

tes = tk.Tk()
tes.withdraw()
# pilih file yang akan dikirim
file = filedialog.askopenfilename(filetypes=(('All files', '.*'),))
loc = os.path.split(file)[0]
nama_file = os.path.split(file)[1]
loc_final = loc + "/"

# buka file yang akan diupload
with open(loc_final+nama_file,'rb') as handle:
    # baca file dan ubah menjadi biner dengan xmlrpc.client.Binary
    data = xmlrpc.client.Binary(handle.read())

# panggil fungsi untuk upload yang ada di server
proxy.file_upload(data)