# import xmlrpc bagian client
import xmlrpc.client
import tkinter as tk
from tkinter import filedialog

# buat proxy untuk mengakses server. Gunakan parameter URL server yang akan diakses berupa IP dan port. Bentuk http://IP:port
proxy = xmlrpc.client.ServerProxy("http://127.0.0.1:8001")

tes = tk.Tk()
tes.withdraw()
# menentukan directory penyimpanan
loc = filedialog.askdirectory()
loc_final = loc + "/"

# file akan didownload ssesuai dengan format yg dikirim  oleh server
with open(loc_final+"hasil_download.*","wb") as handle:
    
    # tulis/isi file hasil_download.*(artinya bisa format apa  aja) dengan hasil dari memanggil fungsi "download" yang berada server
    # ubah file menjadi binary dengan menambahkan .data
    handle.write(proxy.file_download().data)
    
